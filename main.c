#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<fcntl.h>
#include<unistd.h>

const int MAX_SIZE = 4096;

const char *ascii_list[33] = {"NUL", "SOH", "STX", "ETX", "EOT", "ENQ"
, "ACK", "BEL", "BS", "HT", "LF", "VT", "FF", "CR", "SO" ,"SI", "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", " SYN", "ETB", "CAN", "EM", "SUB",
"ESC", "FS", "GS", "RS", "US", "SPACE"};

int main(int argc, char **argv){

	char *buffer = malloc(MAX_SIZE*sizeof(char));
	int n; int count = 0;

	if(argv[1] == "-" || argc == 1){
		//No file parameter
		printf("\nNo file parameter. Enter your 1s & 0s: " );
    		fgets (buffer, MAX_SIZE, stdin);  
		count = strlen(buffer);
		printf("\nInput received.\n" );
	} else {
		//Open the file
		int file_descriptor = open(argv[1], O_RDONLY);

		//Read the file until EOF is reached
		while((n = read(file_descriptor, buffer, MAX_SIZE)) > 0)
			count = n;

		buffer[count++] = '\0';
	}

	printf("\nOriginal  ASCII  Decimal  Parity  T.Error\n");
	printf("--------  -----  -------  ------  -------\n");
	
        int index = 0;
	//Every 8 characters calculate the row
	while(index < count){
		calculateRow(buffer, index);
		index += 8;	
	}

	free(buffer);
	return 0;
}

char convertToAscii(int decimal){	
	char c = decimal;
	return c;
}

int calculateRow(char buffer[], int index){
	char tempBuffer[512];
	int i = 0; int count = 0;
	
	//Store 8 characters into the tempBuffer from the originalBuffer
	//Stop when 8 characters are received or when a null terminating character is reached.
	for(i; i < 8;i++){
		char c = buffer[index+i];
		if(c == '\0') break;
		tempBuffer[i] = buffer[index+i];
		count++;
	}

	//If tempBuffer does not contain 8 characters pad it with 0s
	int j = 0;
	if(count < 8){
		for(j; j <= 8 - count ;j++){
			tempBuffer[count + j - 1] = '0';
		}
	}
	tempBuffer[8] = '\0';
	
	int decimal = calculateDecimal(tempBuffer);
	char c = convertToAscii(decimal);
	
	printf("%s", tempBuffer);

	if(decimal <= 32) printf("    %s", ascii_list[decimal]);
	else if(decimal == 127) printf("  DEL");
	else printf("    %c", c);

	printf("      %d", decimal);

	if(calculateParity(tempBuffer) == 0) printf("     EVEN    FALSE\n");
	else printf("     ODD     TRUE\n");
}

int calculateParity(char buffer[]){
	int i = 0;
	int total = 0;

	for(i;i < 8;i++){
		if(buffer[i] == '1') total+=1;
	}

	if(total%2 == 0) return 0;
	else return 1;
}

int calculateDecimal(char buffer[]){
	
	//Start from the back of the array
	int i = 7;
	int total = 0;
	for(i; i > 0;i--){
		char c = buffer[i];
		if(c == '1') total += power(2,7-i);
	}
	
	return total;
}

int power(int base, int power){
	int i  = 1;
	int result = 1;
	for(i; i <= power;i++){
		result = result * base;
	}
	return result;
}


